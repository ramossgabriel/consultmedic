package consultas;

import pessoas.Paciente;

import java.util.ArrayList;
import java.util.List;

public class Prontuario {
    private Paciente prontuarioPaciente;
    private String dataRegistro;
    private List<Descricao> descricao = new ArrayList<>();

    public Prontuario(Paciente prontuarioPaciente, String dataRegistro) {
        this.prontuarioPaciente = prontuarioPaciente;
        this.dataRegistro = dataRegistro;
        System.out.println("Criação do prontuário do paciente - " +
                            prontuarioPaciente.getNome() +
                            "\n");
    }
}
