package pessoas;

public class Medico extends Funcionario {
    private String crm;
    private String ufCrm;
    private String especialidade;

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getUfCrm() {
        return ufCrm;
    }

    public void setUfCrm(String ufCrm) {
        this.ufCrm = ufCrm;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public void registroInfoMedico(String crm, String ufCrm, String especialidade) {
        setCrm(crm);
        setUfCrm(ufCrm);
        setEspecialidade(especialidade);
    }

    @Override
    public String toString() {
        return "Medico{" +
                " nome='" + nome + '\'' +
                ", endereco='" + endereco + '\'' +
                ", telefone='" + telefone + '\'' +
                ", cpf='" + cpf + '\'' +
                ", dataNascimento='" + dataNascimento + '\'' +
                ", idade=" + idade +
                ", crm='" + crm + '\'' +
                ", ufCrm='" + ufCrm + '\'' +
                ", especialidade='" + especialidade + '\'' +
                '}';
    }

    public void alterarInfoMedico(Medico medico){
        if (medico.getCpf() == this.getCpf()) {
            setCrm(crm);
            setUfCrm(ufCrm);
            setEspecialidade(especialidade);
        }

        System.out.println("Medico não encontrado");
    }
}
