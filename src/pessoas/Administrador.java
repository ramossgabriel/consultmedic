package pessoas;

public class Administrador extends Funcionario{
    private String nomeClinica;
    private String enderecoClinica;
    private String telefoneClinica;


    private String getNomeClinica() {
        return nomeClinica;
    }

    private void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    private String getEnderecoClinica() {
        return enderecoClinica;
    }

    private void setEnderecoClinica(String enderecoClinica) {
        this.enderecoClinica = enderecoClinica;
    }

    private String getTelefoneClinica() {
        return telefoneClinica;
    }

    private void setTelefoneClinica(String telefoneClinica) {
        this.telefoneClinica = telefoneClinica;
    }

    public void inserirInfoClinica(String nomeClinica, String enderecoClinica,
                                   String telefoneClinica){
        setNomeClinica(nomeClinica);
        setEnderecoClinica(enderecoClinica);
        setTelefoneClinica(telefoneClinica);
        System.out.println("Dados da Clínica Inseridos");
    }

    public void alterarInfoClinica(String nomeClinica, String enderecoClinica,
                                   String telefoneClinica){
        setNomeClinica(nomeClinica);
        setEnderecoClinica(enderecoClinica);
        setTelefoneClinica(telefoneClinica);
        System.out.println("Dados da Clínica Inseridos");
    }

    public String getInfoClinica(){
        String info = "Nome da Clinica: "+getNomeClinica()+"\n"
                        + "Endereço: "+getEnderecoClinica()+"\n"
                        + "Telefone: "+getTelefoneClinica()+"\n";
        return info;
    }


}
