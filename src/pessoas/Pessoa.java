package pessoas;

public class Pessoa {
    protected String nome;
    protected String endereco;
    protected String telefone;
    protected String cpf;
    protected String dataNascimento;
    protected int idade;

    public Pessoa(){
        System.out.println("Pessoa Criada");
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public int getIdade() {
        return idade;
    }

    public void registrar(String nome, String endereco,
                          String telefone, String cpf,
                          String dataNascimento, int idade){
        setNome(nome);
        setEndereco(endereco);
        setCpf(cpf);
        setTelefone(telefone);
        setDataNascimento(dataNascimento);
        setIdade(idade);
        System.out.println("Usuario registrado");
    }

    public void alterar(String nome, String endereco,
                          String telefone, String cpf,
                          String dataNascimento, int idade){
        setNome(nome);
        setEndereco(endereco);
        setCpf(cpf);
        setTelefone(telefone);
        setDataNascimento(dataNascimento);
        setIdade(idade);
        System.out.println("Usuario registrado");
    }

    public void consultar(String nome){
        if (verificaPessoa())
            if (getNome() == nome){
                System.out.println("Usuário encontrado\n"+toString());

            }
        else
            System.out.println("Usuario inexistente");
    }

    @Override
    public String toString(){
        String usuario = "\n\nNome: "+getNome()+"\n"
                        +"Endereco: "+getEndereco()+"\n"
                        +"Telefone: "+getTelefone()+"\n"
                        +"Data de Nascimento: "+getDataNascimento()+"\n"
                        +"Idade: "+this.idade+"\n";
        return usuario;
    }

    public boolean verificaPessoa(){
        //System.out.println("Nome: "+getNome());
        if(this.nome==null) return false;
        return true;
    }
}
