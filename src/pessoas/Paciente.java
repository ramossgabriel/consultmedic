package pessoas;

public class Paciente extends Pessoa{
    private String cartaoSus;

    public String getCartaoSus() {
        return cartaoSus;
    }

    public void setCartaoSus(String cartaoSus) {
        this.cartaoSus = cartaoSus;
    }

    public void registrar(String nome, String endereco,
                          String telefone, String cpf,
                          String dataNascimento, int idade,
                          String cartaoSus) {
        setNome(nome);
        setEndereco(endereco);
        setCpf(cpf);
        setTelefone(telefone);
        setDataNascimento(dataNascimento);
        setIdade(idade);
        setCartaoSus(cartaoSus);
        System.out.println("Paciente registrado");
    }

    @Override
    public String toString() {
        return "Paciente{" +
                "cartaoSus='" + cartaoSus + '\'' +
                ", nome='" + nome + '\'' +
                ", endereco='" + endereco + '\'' +
                ", telefone='" + telefone + '\'' +
                ", cpf='" + cpf + '\'' +
                ", dataNascimento='" + dataNascimento + '\'' +
                ", idade=" + idade +
                '}';
    }
}
