package pessoas;

public class Funcionario extends Pessoa {
    private String nomeUsuario;
    private String senha;
    private boolean usuarioLogado;

    private String getNomeUsuario() {
        return nomeUsuario;
    }

    private void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    private String getSenha() {
        return senha;
    }

    private void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(boolean usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public void login(String nomeUsuario, String senha){
        if (validaLoginUser(nomeUsuario,senha)){
            System.out.println("Usuario autenticado");
            setUsuarioLogado(true);
            System.out.println("Usuario: ---------" + this.toString()+"\n"
                    +"Nome de usuário: "+ getNomeUsuario()+"\n"
                    +"Senha: "+getSenha()+"\n"
                    +"Status de usuario: "+(isUsuarioLogado()?"Logado":"Deslogado"));
        }else{
            System.out.println("Erro na autenticação. Tente novamente!\n\n");
        }
    }

    public void atualizaLogin(String nomeUsuario, String senha){
        if (!validaLoginUser(nomeUsuario,senha)){
            System.out.println("Dados diferentes, atualizando...");
            setNomeUsuario(nomeUsuario);
            setSenha(senha);
            System.out.println("Usuario: ---------" + this.toString()+"\n"
                                +"Nome de usuário: "+ getNomeUsuario()+"\n"
                                +"Senha: "+getSenha()+"\n"
                                +"Status de usuario: "+(isUsuarioLogado()?"Logado":"Deslogado"));
        }
    }

    public void registroLogin(String nomeUsuario, String senha){
        if (getNomeUsuario()==nomeUsuario){
            System.out.println("Impossivel registrar - nome de usuario existe no sistema");
        }else{
            setNomeUsuario(nomeUsuario);
            setSenha(senha);
            setUsuarioLogado(false);
            System.out.println("login registrado");
            System.out.println("Usuario: ---------" + this.toString()+"\n"
                    +"Nome de usuário: "+ getNomeUsuario()+"\n"
                    +"Senha: "+getSenha()+"\n"
                    +"Status de usuario: "+(isUsuarioLogado()?"Logado":"Deslogado"));
        }
    }

    public void deslogar(String nomeUsuario, String senha){
        if (validaLoginUser(nomeUsuario,senha) && isUsuarioLogado()) {
            setUsuarioLogado(false);
            System.out.println("Usuario deslogado");
        }else
            System.out.println("Usuario precisa se autenticar, primeiro");
    }

    private boolean validaLoginUser(String nomeUsuario, String senha){
        if ((getNomeUsuario() == nomeUsuario) && (getSenha() == senha)) return true;
        return false;
    }


}
